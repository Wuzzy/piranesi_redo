An evolved but mostly faithful fan remake of the 2022 Minetest Game Jam silver medalist:

### [Piranesi](https://content.minetest.net/packages/cool_beans/piranesi/) by [iarbat](https://content.minetest.net/users/cool_beans/)

This game was originally created as a test case for the [Minetest Definition Ripper](https://gitlab.com/sztest/defripper) mod, but it has since evolved into a full remake of the original game, using all new code, but mostly original assets, schematics, design, and core gameplay.

## Improvements

- The game is fully working and playable from beginning to end.
	- All areas are accessible.
	- Each puzzle is fully functioning, as apparently intended.
	- The game properly keeps state across mulitple sessions / server restarts.
	- Inherited behavior no longer interferes with gameplay (e.g. fire does not spread).
- Items can be placed in most places in most rooms, and are reliably preserved if you leave the area and re-enter later.
- Corrected and simplified UX and inventory.
	- Less left/right-click ambiguity, critical actions are more likely to work with either.
	- Simplified dynamic hotbar-only inventory.
	- Working compass built into HUD.
- More robust map management and navigation.
	- It's no longer possible to "escape" from the map, fall into the endless void, miss triggers, etc.
- Sequential Multiplayer
	- Only one player can play at a time, but players with interact privileges can take turns (first come first served).
	- Additional players who connect are not allowed to interact with the world, but can be managed by an external spectator mod.
- Activated some unused assets.
	- One originally-completely-inaccessible puzzle room, part of the "main quest".
	- One unused room whose purpose was unclear (used as a decorative bonus here).
	- Music distributed with the original game but never actually played.
- Improved sensory effects.
	- Visuals, e.g. for item pickup and breaking nodes, completing puzzles.
	- Sound overhaul, including feedback, ambiance, and musical cues.
- Colorblindness support, up to full monochromacy, via HUD tips.
- Some random enhancements that probably don't dramatically alter gameplay.
	- The ability to pick up, carry, and move books and clues.
	- Architectural improvements, like reunifying dual item/nodes into a single definition.
	- Room order is now deterministic and pseudorandom to make play-throughs repeatable, for speedruns or exact walkthroughs.

## Compromises

- Some design choices were made where the original intent was unclear, or where obvious mechanics had less-than-obvious problems that needed to be worked around.
- The use of Definition Ripper for all materials indiscriminately, including the original Piranesi ones, causes some architectural extensibility friction.
- There is no longer a distinction between "digging" a node and "applying a tool" to the node, so all "dig times" must be instant.
- Node/item placement rules are a bit byzantine for technical reasons, so mostly just the floor and some tables are usable for storing most things, though this is still plenty of space.
	- Not all things can be put back where they were found, e.g. keys cannot be re-stuck to walls.
- Some lighting adjustments were necessary, due to the game apparently having been designed for a different version of Minetest that may have used a different light curve model.