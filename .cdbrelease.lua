-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "piranesi_redo",
	dev_state = "ACTIVELY_DEVELOPED",
	version = stamp .. "-$Format:%h$",
	type = "game",
	title = "Piranesi Restoration Project",
	short_description = "Polished fan remake of Piranesi, the space-bending puzzle game",
	tags = {"puzzle", "singleplayer"},
	content_warnings = {},
	license = "MIT",
	media_license = "CC-BY-SA-4.0",
	long_description = readtext("README.md"),
	repo = "https://gitlab.com/sztest/piranesi_redo",
	issue_tracker = "https://gitlab.com/sztest/piranesi_redo/-/issues",
	maintainers = {"Warr1024"},
	screenshots = {readbinary("menu/background.png")}
}

-- luacheck: pop
