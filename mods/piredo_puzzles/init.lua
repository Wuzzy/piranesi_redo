-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, include, minetest
    = get_mod_api, include, minetest
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()

function api.magicsound(pos)
	return minetest.sound_play("xdecor_enchanting", {pos = pos}, true)
end

function api.magic(pos, sound)
	if sound then api.magicsound(pos) end
	return minetest.add_particlespawner({
			amount = 50,
			time = 1,
			pos = pos,
			radius = {min = 0, max = 0.5},
			acc = {x = 0, y = 3, z = 0},
			exptime = {min = 1, max = 3},
			size = {min = 0.5, max = 2},
			glow = 10,
			texture = "piranesi_magic_particle.png",
		})
end

include("doors")
include("digging")
include("gears")
include("flowerpots")
include("potions")
include("crowns")
include("candles")
include("clock")
include("finalaltar")
