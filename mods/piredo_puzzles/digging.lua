-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_items, posrel
    = minetest, piredo_items, posrel
-- LUALOCALS > ---------------------------------------------------------

local function breaknode(pos, node)
	minetest.remove_node(pos)
	piredo_items.digeffect(pos, node)
	minetest.check_for_falling(posrel(pos)(0, 1, 0))
end

minetest.override_item("piredo_terrain:piranesi__dirt_with_grass", {
		on_whack_shovel = breaknode
	})

minetest.override_item("piredo_terrain:default__dirt_with_coniferous_litter", {
		on_whack_shovel = function(pos, node)
			local rel = posrel(pos)

			local an = minetest.get_node(rel(0, 1, 0))
			local adef = minetest.registered_nodes[an.name]
			if not (adef and adef.air_equivalent) then return end

			local bn = minetest.get_node(rel(0, -1, 0))
			if bn.name == "ignore" then return end
			local bdef = minetest.registered_nodes[bn.name]
			if (not bdef) or (bdef.air_equivalent) then
				minetest.set_node(rel(0, -1, 0), {
						name = "piredo_terrain:default__dirt"
					})
			end

			return breaknode(pos, node) or nil
		end
	})

minetest.override_item("piredo_terrain:piranesi__pine_tree", {
		on_whack_axe = breaknode
	})
minetest.override_item("piredo_terrain:morelights_vintage__lantern_f", {
		groups = {falling_node = 1}
	})
