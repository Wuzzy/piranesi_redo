-- LUALOCALS < ---------------------------------------------------------
local ItemStack, addgroups, minetest, pairs, piredo_items,
      piredo_player, posrel
    = ItemStack, addgroups, minetest, pairs, piredo_items,
      piredo_player, posrel
-- LUALOCALS > ---------------------------------------------------------

addgroups("piredo_terrain:piranesi__hot_stone", "hot_stone")

for item in pairs({
		piranesi__swordblock = true,
		piranesi__plateblock = true,
		piranesi__keyblock_metal = true,
		piranesi__gear = true,
	}) do
	addgroups("piredo_terrain:" .. item, "metallic")
end

minetest.override_item("piredo_terrain:piranesi__pot", {
		on_apply_metallic = function(pos, node, stack)
			local rel = posrel(pos)
			local bn = minetest.get_node(rel(0, -1, 0))
			if minetest.get_item_group(bn.name, "hot_stone") > 0 then
				piredo_player.discover("solve:melt:" .. stack:get_name())
				minetest.sound_play("xdecor_boiling_water",
					{pos = pos}, true)
				node.name = "piredo_terrain:piranesi__pot_full"
				minetest.swap_node(pos, node)
				stack:take_item(1)
				return stack
			end
			return false
		end
	})

minetest.override_item("piredo_terrain:piranesi__mold", {
		on_apply_pot_full = function(pos, node)
			piredo_player.discover("solve:gear_mold")
			minetest.sound_play("xdecor_boiling_water",
				{pos = pos}, true)
			node.name = "piredo_terrain:piranesi__mold_full"
			minetest.swap_node(pos, node)
			return "piredo_terrain:piranesi__pot"
		end
	})

minetest.register_abm({
		nodenames = {"piredo_terrain:piranesi__mold_full"},
		interval = 2,
		chance = 1,
		action = function(pos, node)
			local bpos = posrel(pos)(0, -1, 0)
			local bnode = minetest.get_node(bpos)
			if bnode.name ~= "piredo_terrain:piranesi__ice" then return end
			node.name = "piredo_terrain:piranesi__mold_done"
			piredo_player.discover("solve:gear_quench")
			minetest.swap_node(pos, node)
			minetest.sound_play("piredo_puzzles_nodecore_hiss",
				{pos = pos, gain = 0.5}, true)
			minetest.add_particlespawner({
					amount = 50,
					time = 1,
					pos = pos,
					radius = {min = 0, max = 0.5},
					acc = {x = 0, y = 3, z = 0},
					exptime = {min = 1, max = 3},
					size = {min = 0.5, max = 2},
					glow = 10,
					texture = "default_item_smoke.png^[opacity:64",
				})
		end
	})

do
	local gear = "piredo_terrain:piranesi__gear"
	minetest.override_item("piredo_terrain:piranesi__mold_done", {
			on_punch = function(pos, node, player)
				piredo_player.discover("solve:gear_extract")
				local stack = ItemStack(gear)
				stack = player:get_inventory():add_item("main", stack)
				if not stack:is_empty() then return end
				node.name = "piredo_terrain:piranesi__mold"
				minetest.swap_node(pos, node)
				piredo_items.pickupeffect(pos, {name = gear})
			end
		})
end

for i = 1, 2 do
	minetest.override_item("piredo_terrain:piranesi__machine_gear_" .. i .. "_e", {
			on_apply_gear = function(pos, node, stack)
				piredo_player.discover("solve:gear_install")
				minetest.sound_play("doors_steel_door_open", {pos = pos}, true)
				node.name = "piredo_terrain:piranesi__machine_gear_" .. i
				minetest.swap_node(pos, node)
				stack:take_item(1)
				return stack
			end
		})
	addgroups("piredo_terrain:piranesi__machine_gear_" .. i, "machine_ready")
end

do
	local function machine_ready(pos)
		return minetest.get_item_group(minetest.get_node(pos).name,
		"machine_ready") > 0
	end
	minetest.override_item("piredo_terrain:piranesi__machine_lever", {
			on_punch = function(pos)
				local rel = posrel(pos)
				if not machine_ready(rel(0, 1, 0)) then return end
				if not machine_ready(rel(0, 1, 1)) then return end
				if not machine_ready(rel(0, 0, 1)) then return end
				piredo_player.discover("solve:machine_start")

				minetest.sound_play("doors_fencegate_open",
					{pos = pos}, true)
				minetest.sound_play("piredo_puzzles_sztest_gears_start",
					{pos = pos}, true)

				minetest.swap_node(rel(0, 1, 0),
					{name = "piredo_terrain:piranesi__machine_gear_2_a"})
				minetest.swap_node(rel(0, 1, 1),
					{name = "piredo_terrain:piranesi__machine_gear_2_a"})
				minetest.swap_node(rel(0, 0, 1),
					{name = "piredo_terrain:piranesi__machine_gear_1_a"})

				minetest.swap_node(rel(1, 0, 0),
					{name = "piredo_terrain:piranesi__green_a"})
				minetest.swap_node(rel(1, 0, 1),
					{name = "piredo_terrain:piranesi__blue_a"})
				minetest.swap_node(rel(1, 1, 1),
					{name = "piredo_terrain:piranesi__yellow_a"})
				minetest.swap_node(rel(1, 1, 0),
					{name = "piredo_terrain:piranesi__red_a"})
			end
		})
end
