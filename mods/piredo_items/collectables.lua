-- LUALOCALS < ---------------------------------------------------------
local addgroups, minetest, pairs
    = addgroups, minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local keyitems = {
	piranesi__pot = "",
	piranesi__pot_full = "",
	piranesi__mold = "",
	piranesi__mold_full = "",
	piranesi__gear = "",
	piranesi__axeblock = "Axe",
	piranesi__shovelblock = "Shovel",
	piranesi__bottle_red_block = "Red Liquid",
	piranesi__bottle_green_block = "Green Liquid",
	piranesi__bottle_yellow_block = "Yellow Liquid",
	piranesi__bottle_blue_block = "Blue Liquid",
	piranesi__bottle_purple = "Purple Liquid",
	piranesi__bottle_black = "Black Liquid",
	piranesi__bottle_block = "Bottle",
	piranesi__crownblock = "Crown",
	piranesi__crownblock1 = "Crown",
	piranesi__crownblock2 = "Crown",
	piranesi__swordblock = "Sword",
	piranesi__plateblock = "Pewter Plate",
	piranesi__keyblock_gold = "Golden Key",
	piranesi__keyblock_metal = "Metal Key",
	piranesi__keyblock_black = "Black Key",
	piranesi__dandelion_white = "",
	piranesi__geranium = "",
	piranesi__chrysanthemum_green = "",
	piranesi__dandelion_yellow = "",
	piranesi__rose = "",
	piranesi__lighterblock = "Lighter",
	piranesi__coin_totem = "",
	piranesi__chess_totem = "",
	piranesi__neck_totem = "",
	piranesi__time_totem = "",
}
for k, v in pairs(keyitems) do
	local name = "piredo_terrain:" .. k
	addgroups(name, "puzzle_item")
	if v and v ~= "" then minetest.override_item(name, {description = v}) end
end

local allitems = {
	-- general utility
	xdecor__candle = "",

	-- distractors
	default__pine_sapling = "",
	default__acacia_sapling = "",
	default__aspen_sapling = "",
	default__apple = "",
	farming__salt = "White Granules",
	farming__pepper_ground = "Black Granules",
	farming__rose_water = "Pink Liquid",
	farming__salt_crystal = "Crystal",
	farming__garlic_braid = "",
	farming__bottle_ethanol = "Cloudy Liquid",
	farming__soy_sauce = "Brown Liquid",
	farming__vanilla_extract = "Russet Liquid",
}
for k, v in pairs(keyitems) do allitems[k] = v end

for k, v in pairs(allitems) do
	local name = "piredo_terrain:" .. k
	addgroups(name, "collectable")
	if v and v ~= "" then minetest.override_item(name, {description = v}) end
end
