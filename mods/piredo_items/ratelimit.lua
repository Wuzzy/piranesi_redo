-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, minetest
    = get_mod_api, minetest
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()

local now = 0
minetest.register_globalstep(function(dtime) now = now + dtime end)

local cooldown = 0

local skip
api.ratelimitskip = function() skip = true end

local function helper(...)
	if skip then return ... end
	cooldown = now + 0.25
	return ...
end

api.ratelimit = function(func)
	return function(...)
		if cooldown > now then return end
		skip = nil
		return helper(func(...))
	end
end
