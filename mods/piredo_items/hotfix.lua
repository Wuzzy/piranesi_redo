-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

minetest.override_item("piredo_terrain:piranesi__lighterblock", {
		inventory_image = "piranesi_lighter_open.png",
		wield_image = "piranesi_lighter_open.png"
	})

-- For some reason the forest has 2 different kinds of white flowers.
-- Get rid of one.
minetest.unregister_item("piredo_terrain:flowers__dandelion_white")
minetest.register_alias("piredo_terrain:flowers__dandelion_white", "air")
minetest.register_alias("flowers:dandelion_white", "air")

-- Fix bottles, especially wrong-colored black potion from original.
local potionbox = minetest.registered_items["piredo_terrain:farming__rose_water"].selection_box
minetest.override_item("piredo_terrain:piranesi__bottle_black", {
		inventory_image = "piranesi_bottle_black.png",
		tiles = {"piranesi_bottle_black.png"},
		drawtype = "plantlike",
		selection_box = potionbox
	})
minetest.override_item("piredo_terrain:piranesi__bottle_purple", {
		drawtype = "plantlike",
		selection_box = potionbox
	})
