-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, minetest
    = get_mod_api, minetest
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()
local modstore = minetest.get_mod_storage()

local stopkey = "timer_stopped"
local stopped = modstore:get_string(stopkey) ~= ""

local timerkey = "total_elapsed"
local timer = modstore:get_float(timerkey)

function api.stop_timer()
	stopped = true
	modstore:set_string(stopkey, "1")
end

function api.is_timer_stopped() return stopped end

minetest.register_globalstep(function(dtime)
		if not stopped then
			timer = timer + dtime
			modstore:set_float(timerkey, timer)
		end
		api.timer = timer
	end)
