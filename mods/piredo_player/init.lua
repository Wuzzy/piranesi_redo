-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("playerapi")
include("discover")
include("timer")
include("screenfade")
include("endcredits")
include("savepos")
include("playerinit")
include("compass")
include("colorhuds")
include("wieldtip")
include("model")
include("invadd")
include("hotbar")
