-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, ipairs, math, minetest, piredo_player, string, table
    = get_mod_api, ipairs, math, minetest, piredo_player, string, table
local math_floor, string_format, string_match, table_concat
    = math.floor, string.format, string.match, table.concat
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()
local modname = minetest.get_current_modname()

local endpos

function api.is_endcredits() return endpos and true end
function api.set_endcredits(pos) endpos = pos end

minetest.register_entity(modname .. ":playerhold", {
		initial_properties = {
			static_save = false,
			is_visible = true,
			visual = "cube",
			visual_size = {x = 2, y = 4, z = 2},
			backface_culling = false,
			textures = {
				"[combine:1x1^[noalpha",
				"[combine:1x1^[noalpha",
				"[combine:1x1^[noalpha",
				"[combine:1x1^[noalpha",
				"[combine:1x1^[noalpha",
				"[combine:1x1^[noalpha",
			},
			pointable = false,
			physical = false
		}
	})

local function timefmt(n)
	local s = string_format("%0.3f", n % 60)
	if n < 60 then return s end
	if (n % 60) < 10 then s = "0" .. s end
	local m = math_floor(n / 60)
	if m < 60 then return m .. ":" .. s end
	local h = math_floor(m / 60)
	m = m % 60
	m = (m < 10) and ("0" .. m) or ("" .. m)
	return h .. ":" .. (m % 60) .. ":" .. s
end

local function show_credit_hud(player)
	local credit_text = {
		"",
		"",
		"",
		"",
		"Programming - Warr1024",
		"Writing and Design - iarbat",
		"See LICENSE.txt for all contributors",
		"",
		"https://gitlab.com/sztest/piranesi_redo",
		"",
		"Time",
		timefmt(api.timer),
		"",
		"Completion",
		math_floor((api.score / 90) * 100 + 0.5) .. "%",
	}

	local titlesize = 0.262295081967
	player:hud_add({
			hud_elem_type = "image",
			position = {x = 0.5, y = 0.5},
			offset = {x = 0, y = -8 * #credit_text},
			scale = {x = titlesize, y = titlesize},
			text = "piredo_player_header.png",
			z_index = -100,
		})

	for i = 1, #credit_text do
		local lines = {}
		for j = 1, #credit_text do
			lines[#lines + 1] = i == j and credit_text[j] or ""
		end
		local text = table_concat(lines, "\n")
		if string_match(text, "%S") then
			player:hud_add({
					hud_elem_type = "text",
					position = {x = 0.5, y = 0.5},
					offset = {x = 0, y = 32},
					text = text,
					number = 0xffffff,
					z_index = -100
				})
		end
	end
end

local function check_end_credits(player)
	if not endpos then return end

	if player:get_properties().visual ~= "sprite" then
		player:set_properties({
				visual = "sprite",
				textures = {
					"[combine:1x1",
					"[combine:1x1",
				}
			})
		player:hud_set_flags({
				crosshair = false,
				wielditem = false,
			})
		api.hide_compass(player)
	end

	if not player:get_attach() then
		piredo_player.screenfade(player, true)
		local obj = minetest.add_entity(endpos,
			modname .. ":playerhold")
		if obj then
			player:set_attach(obj)
			show_credit_hud(player)
			piredo_player.screenfade(player)
		end
	end
end

minetest.register_globalstep(function()
		for _, player in ipairs(minetest.get_connected_players()) do
			check_end_credits(player)
		end
	end)
