-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, minetest
    = get_mod_api, minetest
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()

local showtime = 2

local now = 0
minetest.register_globalstep(function(dtime) now = now + dtime end)

local wielddata = api.create_player_data()
api.register_playerstep(function(player, _, pname)
		local data = wielddata(pname)

		local text = player:get_wielded_item():get_description() or ""
		local idx = player:get_wield_index()
		if text == data.rawtext and idx == data.idx then
			if now > data.time + showtime then
				text = ""
			end
		else
			data.time = now
			data.rawtext = text
			data.idx = idx
		end

		if not data.hud then
			data.hud = player:hud_add({
					hud_elem_type = "text",
					position = {x = 0.5, y = 1},
					offset = {x = 0, y = -128},
					alignment = {x = 0, y = -1},
					number = 0xFFFFFF,
					text = text
				})
			data.hudtext = text
		elseif data.hudtext ~= text then
			player:hud_change(data.hud, "text", text)
			data.hudtext = text
		end
	end)
