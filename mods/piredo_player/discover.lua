-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, ipairs, minetest, pairs, string
    = get_mod_api, ipairs, minetest, pairs, string
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

-- TOTAL POSSIBLE SCORE:
-- 10 clues to read
-- 32 critical items to collect
-- 28 rooms to visit
-- 8 forest machine puzzles
-- 12 other puzzles
-- TOTAL 90
-- (+1 for the bonus room)

local api = get_mod_api()

local modstore = minetest.get_mod_storage()

local scorekey = "discovered"
local status = modstore:get_string(scorekey)
status = status and status ~= "" and minetest.deserialize(status) or {}

local function calcscore()
	local n = 0
	for _ in pairs(status) do n = n + 1 end
	api.score = n
end
calcscore()

function api.discover(label)
	if status[label] then return end
	status[label] = true
	calcscore()
	modstore:set_string(scorekey, minetest.serialize(status))
	minetest.log("action", string_format(
			"player discovered %q, total score %d",
			label, api.score))
end

api.register_playerstep(function(player)
		for _, stack in ipairs(player:get_inventory():get_list("main")) do
			local name = stack:get_name()
			if name ~= "" and minetest.get_item_group(name,
			"puzzle_item") > 0 then
				api.discover("inv:" .. name)
			end
		end
	end)
