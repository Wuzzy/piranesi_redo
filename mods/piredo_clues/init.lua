-- LUALOCALS < ---------------------------------------------------------
local addgroups, minetest, piredo_player, string
    = addgroups, minetest, piredo_player, string
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

local fse = minetest.formspec_escape

local function register_clue(basename, desc, subdesc, form)
	local fullname = "piredo_terrain:piranesi__" .. basename
	addgroups(fullname, "clue", "collectable")
	local olddef = minetest.registered_items[fullname]
	minetest.override_item(fullname, {
			description = desc,
			inventory_image = olddef.tiles[1],
			wield_image = olddef.tiles[1],
			wield_scale = {x = 1, y = 1, z = olddef.drawtype == "signlike" and 1 or 2},
			on_use = function(stack, player)
				if not player then return end
				piredo_player.discover("read:" .. fullname)
				minetest.sound_play("piredo_clues_paper",
					{pos = player:get_pos()}, true)
				minetest.show_formspec(player:get_player_name(), "", form)
				if subdesc then
					stack:get_meta():set_string("description",
						string_format("%s - %s", desc, subdesc))
					return stack
				end
			end,
			pointable = true
		})
end

local function formstart(w, h)
	return "size[" .. w .. "," .. h .. "]"
	.. "style_type[image_button;border=false;bgcolor=#00000000]"
	.. "image_button_exit[0,0;" .. w .. "," .. h
	.. ";" .. fse("[combine:1x1") .. ";close;]"
end

register_clue("star_chart", "Star Chart", nil, formstart(20, 10)
	.. "image[0,0;25,12.5;piranesi_clue_star_chart.png]")
register_clue("clue_manu", "Manuscript", nil, formstart(21, 15) .. "image[0,0;26.2,18.1125;"
	.. "piranesi_clue_manuscript.png]")

local function register_clue_paper(basename, subdesc, img)
	img = "[combine:512x682:0,0=piranesi_back_paper.png"
	.. ":0,0=piranesi_clue_" .. img .. ".png"
	local ratio = 682/512
	local w = 20
	local h = ratio * w
	local size = (w * 1.25) .. "," .. (h * 1.25)
	return register_clue(basename, "Paper", subdesc, formstart(w, h)
		.. "image[0,0;" .. size .. ";" .. fse(img) .. "]")
end

register_clue_paper("clue_1", "Direction", "starting")
register_clue_paper("clue_2", "Machine", "machine")
register_clue_paper("clue_3", "Clock", "clock")
register_clue_paper("clue_4", "Furnaces", "forge")

local function register_clue_book(basename, subdesc, img1, img2)
	local back = "[combine:512x682:0,0=piranesi_back_book.png"
	img1 = back .. ":0,0=piranesi_clue_" .. img1 .. ".png"
	img2 = back .. (img2 and (":0,0=piranesi_clue_"
			.. img2 .. ".png") or "")
	local ratio = 682/512
	local w = 10
	local h = ratio * w
	local size = (w * 1.25) .. "," .. (h * 1.25)
	return register_clue(basename, "Book", subdesc, formstart(w * 2, h)
		.. "image[0,0;" .. size .. ";" .. fse(img1)
		.. "]image[" .. w .. ",0;" .. size .. ";" .. fse(img2) .. "]")
end

register_clue_book("book", "Study", "study_1", "study_2")
register_clue_book("book1", "Folklore", "folklore_1", "folklore_2")
register_clue_book("book2", "Cooking", "pot")
register_clue_book("book3", "Poem", "final")
