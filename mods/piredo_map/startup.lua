-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, math, minetest, piredo_player, posrel
    = get_mod_api, math, minetest, piredo_player, posrel
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()

do
	local function startmap(rel)
		api.putschem(rel(-8, -5, -10), "outside")
		api.putschem(rel(2, -5, 30), "hall_z_plus")
	end
	piredo_player.register_player_init(function(player)
			api.statedb.daytime = 0.35 + math_random() * 0.1
			api.savedb()
			local rel = posrel(player:get_pos(), true)
			startmap(rel)
			minetest.after(0.5, startmap, rel)
			player:set_look_horizontal(5.55189235059)
			player:set_look_vertical(-0.256563400043)
		end)
end
