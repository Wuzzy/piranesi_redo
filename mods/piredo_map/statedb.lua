-- LUALOCALS < ---------------------------------------------------------
local get_mod_api, minetest
    = get_mod_api, minetest
-- LUALOCALS > ---------------------------------------------------------

local api = get_mod_api()

local modstore = minetest.get_mod_storage()
local s = modstore:get_string("state") or ""
local statedb = s and s ~= "" and minetest.deserialize(s) or {}
api.statedb = statedb

api.savedb = function()
	modstore:set_string("state", minetest.serialize(statedb))
end
