-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs
    = minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

for k, v in pairs({
		piranesi__trigger_z_plus = {
			piredo_map_trigger_move = {dir = "N", x = -16, y = -1, z = 11}
		},
		piranesi__trigger_z_minus = {
			piredo_map_trigger_move = {dir = "S", x = -16, y = -1, z = -29}
		},
		piranesi__trigger_x_plus = {
			piredo_map_trigger_move = {dir = "E", x = 11, y = -1, z = -3}
		},
		piranesi__trigger_x_minus = {
			piredo_map_trigger_move = {dir = "W", x = -29, y = -1, z = -3}
		}
	}) do
	minetest.override_item("piredo_terrain:" .. k, v)
end

-- Obsolete trigger exit; we exit movement triggers automatically now, which
-- makes the operation atomic and prevents weirdness when walking back and
-- forth through the same hallway rapidly.
minetest.unregister_item("piredo_terrain:piranesi__trigger_spacer")
minetest.register_alias("piredo_terrain:piranesi__trigger_spacer", "air")
minetest.register_alias("piranesi:trigger_spacer", "air")
