-- LUALOCALS < ---------------------------------------------------------
local addgroups, math, minetest, pairs, posrel
    = addgroups, math, minetest, pairs, posrel
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local ambgroup = "ambiance"
local liqgroup = "ambiance_liquid"

minetest.register_abm({
		nodenames = {"group:" .. ambgroup},
		interval = 1,
		chance = 1,
		action = function(pos, node)
			local def = minetest.registered_items[node.name]
			local chance = def and def.groups and def.groups[ambgroup]
			if chance > 1 and math_random(1, chance) ~= 1 then return end

			if def and def.groups and def.groups[liqgroup] then
				local apos = posrel(pos)(0, 1, 0)
				local anode = minetest.get_node(apos)
				local adef = minetest.registered_nodes[anode.name]
				if not (adef and adef.air_equivalent) then return end
			end

			local spec = def and def.sounds and def.sounds[ambgroup]
			if not spec then return end

			local param = {}
			for k, v in pairs(spec) do param[k] = v end
			param.pos = pos
			param.gain = (param.gain or 1) / 5
			param.max_hear_distance = 8
			minetest.after(math_random(), function()
					return minetest.sound_play(
						param.name, param, true)
				end)
		end
	})

local function setambiance(nodename, chance, soundname, gain, pitch)
	local def = minetest.registered_nodes[nodename]
	local groups = {}
	for k, v in pairs(def.groups) do groups[k] = v end
	groups.ambiance = chance or 1
	local sounds = {}
	for k, v in pairs(def.sounds) do sounds[k] = v end
	if not soundname then
		local fallback = def.sounds.footstep or def.sounds.dig or def.sounds.dug
		soundname = fallback and fallback.name
		if not soundname then return end
		gain = (gain or 1) * (fallback.gain or 1)
	end
	sounds[ambgroup] = {
		name = soundname,
		gain = gain or 1,
		pitch = pitch or 1,
	}
	return minetest.override_item(nodename, {groups = groups, sounds = sounds})
end

addgroups("piredo_terrain:default__water_source", liqgroup)
setambiance("piredo_terrain:default__water_source", 25, nil, 0.25, 2)
addgroups("piredo_terrain:default__water_flowing", liqgroup)
setambiance("piredo_terrain:default__water_flowing", 5)
addgroups("piredo_terrain:default__lava_source", liqgroup)
setambiance("piredo_terrain:default__lava_source", 10)
addgroups("piredo_terrain:default__lava_flowing", liqgroup)
setambiance("piredo_terrain:default__lava_flowing", 5)

setambiance("piredo_terrain:piranesi__cauldron", 1, "xdecor_boiling_water")
setambiance("piredo_terrain:piranesi__cauldron_purple", 1, "xdecor_boiling_water")
setambiance("piredo_terrain:piranesi__cauldron_black", 1, "xdecor_boiling_water")
setambiance("piredo_terrain:piranesi__machine_gear_1_a", 5, "piredo_puzzles_sztest_gears")
setambiance("piredo_terrain:piranesi__machine_gear_2_a", 5, "piredo_puzzles_sztest_gears")
setambiance("piredo_terrain:fire__basic_flame", 10, "fire_small", 0.5)
setambiance("piredo_terrain:fire__permanent_flame", 10, "fire_small", 0.5)
